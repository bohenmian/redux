import { combineReducers } from 'redux';
import productReducer from './product/reducers/productReducer';
import userProfileReducer from "./product/reducers/userProfileReducer";
import userProfileLikeReducer from "./product/reducers/userProfileLikeReducer";
export default combineReducers({
  product: productReducer, profile: userProfileReducer, like: userProfileLikeReducer
});
