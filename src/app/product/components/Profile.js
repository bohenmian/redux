import React from 'react';
import {userProfileLike} from "../actions/userActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class Details extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.userProfileLike({"userProfileId": 1, "userName": "anonymous user"})
    }

    render() {
        const {disable, liked} = this.props.likeBody;
        return (<div>
            <h1>User Profile</h1>
            <div>
                <label>User Name: </label>
                <span>{this.props.name}</span>
            </div>
            <div>
                <label>Gender: </label>
                <span>{this.props.gender}</span>
            </div>
            <div>
                <label>Description: </label>
                <span>{this.props.description}</span>
            </div>

            <div>
                <Link to='/'>&lt; Back to Home</Link>
                <button disabled={disable} onClick={this.handleClick}>{liked}</button>
            </div>
        </div>);
    }
}

const mapStateToProps = state => ({
    likeBody: state.like.likeBody
});

const mapDispatchToProps = dispatch => bindActionCreators({
    userProfileLike
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Details);

