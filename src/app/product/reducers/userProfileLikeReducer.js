const initState = {
    likeBody: {
        disable: false,
        liked: "Like"
    }
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'USER_PROFILE_LIKE':
            return {
                ...state,
                likeBody: action.payload
            };
        default:
            return state
    }
};
