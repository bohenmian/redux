export const getUserProfile = (id) => (dispatch) => {
    fetch(`http://localhost:8080/api/user-profiles/${id}`)
        .then(response => response.json())
        .then((response) => {
            dispatch({
                type: 'GET_USER_PROFILE',
                userProfile: response
            })
        });
};

export const userProfileLike = (body) => (dispatch) => {
    fetch('http://localhost:8080/api/like', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(() => {
        dispatch({
            type: 'USER_PROFILE_LIKE',
            payload: {
                liked: 'Liked',
                disable: true
            }
        })
    });
};
