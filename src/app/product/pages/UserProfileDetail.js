import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getUserProfile} from "../actions/userActions";
import Profile from "../components/Profile";

class UserProfileDetail extends React.Component {

    componentDidMount() {
        this.props.getUserProfile(this.props.match.params.id);
    }

    render() {
        const {name, gender, description} = this.props.userProfile;
        return (
            <div className="">
                <Profile name={name} gender={gender} description={description}/>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    userProfile: state.profile.userProfile
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getUserProfile
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileDetail);

